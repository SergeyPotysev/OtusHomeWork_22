using System.Globalization;
using System.IO;
using CsvHelper;
using Otus.Teaching.Concurrency.Import.Core.Generators;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly long _dataCount;

        public CsvGenerator(string fileName, long dataCount)
        {
            _fileName = fileName + ".csv";
            _dataCount = dataCount;
        }
        
        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            using (var writer = new StreamWriter(_fileName))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(customers);
            }
        }

    }
}