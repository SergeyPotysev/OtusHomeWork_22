using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using System;

namespace Otus.Teaching.Concurrency.Import.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // ��������� ������ ����������� �� ����� ������������
            string connection = Configuration.GetConnectionString("ElasticConnection");

            // ���� �� ������� ������ ������ �� �������?
            bool needClearIndex = Convert.ToBoolean(Configuration.GetSection("ClearIndex").Value);

            // �������� ������� �� ElasticSearch
            var elastic = new CustomerRepository(connection);

            // �������� ������ ������
            if(needClearIndex)
                elastic.ClearIndex();

            services.AddScoped<ICustomerRepository>(repo => elastic);

            services.AddControllers();
        }


        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
