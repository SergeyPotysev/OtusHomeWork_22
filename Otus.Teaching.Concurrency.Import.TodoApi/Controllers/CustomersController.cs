﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly ICustomerRepository _customerRepository;

        public CustomersController(ILogger<CustomersController> logger, ICustomerRepository customerRepository)
        {
            _logger = logger;
            _customerRepository = customerRepository;
        }


        /// <summary>
        /// Получение всех пользователей.  GET: api/Customers
        /// </summary>
        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            return _customerRepository.GetAllCustomers();
        }


        /// <summary>
        /// Получение пользователя по Id.  GET: api/Customers/5
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var customer = await _customerRepository.GetCustomerAsync(id);
            if (customer == null)
                return NotFound();
            else
                return Ok(customer);
        }


        /// <summary>
        /// Добавляет пользователя в базу. POST: api/Customers
        /// </summary>
        /// <param name="customer">Данные пользователя.</param>
        [HttpPost]
        public async Task<IActionResult> AddAsync(Customer customer)
        {
            if (customer == null)
            {
                return BadRequest(customer);
            }
            var found = await _customerRepository.GetCustomerAsync(customer.Id);
            if (found == null)
            {
                bool result = await _customerRepository.AddCustomerAsync(customer);
                if (result)
                    return Ok(customer);
            }
            return Conflict();
        }

    }
}
