using Nest;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// ������ ���� ������ Elasticsearch.
        /// </summary>
        private readonly ElasticClient _client;

        /// <summary>
        /// ��������� ���������� � ��.
        /// </summary>
        private readonly ConnectionSettings _settings;
       

        public CustomerRepository(string �onnString)
        {
            try
            {
                // ������ ��� ����������� ����� ������, ���� ��� ���. 
                _settings = new ConnectionSettings(new UriBuilder(�onnString).Uri).DefaultIndex("customers");
                _client = new ElasticClient(_settings);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Environment.Exit(-1);
            }
        }


        /// <summary>
        /// ���������� ������� � ������.
        /// </summary>
        /// <param name="customer">������ ������.</param>
        public void AddCustomer(Customer customer)
        {
            IndexResponse response = _client.IndexDocument(customer);
            if (!response.IsValid)
            {
                throw new ValidationException("Error writing to the database");
            }
        }


        /// <summary>
        /// ���������� ������������ � ������ (async).
        /// </summary>
        public async Task<bool> AddCustomerAsync(Customer customer)
        {
            IndexResponse response = await _client.IndexDocumentAsync(customer);
            if (response.IsValid)
                return true;
            else
                return false;
        }


        /// <summary>
        /// ��������� ������������ �� Id (async).
        /// </summary>
        /// <param name="id">������������� ������������.</param>
        public async Task<Customer> GetCustomerAsync(int id)
        {
            GetResponse<Customer> response = await _client.GetAsync<Customer>(id.ToString());
            if (response.IsValid)
            {
                Customer customer = new Customer
                {
                    Id = Convert.ToInt32(response.Source.Id),
                    FullName = response.Source.FullName,
                    Email = response.Source.Email,
                    Phone = response.Source.Phone
                };
                return customer;
            }
            else
                return null;
        }


        /// <summary>
        /// ��������� ���� �������������.
        /// </summary>
        public IEnumerable<Customer> GetAllCustomers()
        {
            var response = _client.Search<Customer>(s => s.Index("customers")).Documents;
            if (response.Count == 0)
                return null;
            else
                return response;
        }


        /// <summary>
        /// �������� �������.
        /// </summary>
        /// <returns>��������� ��������.</returns>
        public bool DropIndex(string index)
        {
            return _client.Indices.Delete(index).IsValid;
        }


        /// <summary>
        /// �������� �� ������� ���� ���������� � ����� Customer.
        /// </summary>
        /// <returns>��������� ��������.</returns>
        public bool ClearIndex()
        {
            return _client.DeleteByQuery<Customer>(del => del.Query(q => q.QueryString(qs => qs.Query("*")))).IsValid;
        }


        /// <summary>
        /// �������� ������� � ���������� � ������. 
        /// </summary>
        /// <param name="csv">������ � ����������� � ������� � CSV-�������.</param>
        public void AddCsv(string csv)
        {
            string[] parts = csv.Split(",");
            Customer record = new Customer
            {
                Id = int.TryParse(parts[0], out int number) ? number : 0,
                FullName = parts[1],
                Email = parts[2],
                Phone = parts[3]
            };
            AddCustomer(record);
        }
    }
}