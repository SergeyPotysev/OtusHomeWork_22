﻿using Newtonsoft.Json;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Core.Entities;
using System;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.Client.Settings;

namespace Otus.Teaching.Concurrency.Import.Client
{
    public class ConsoleClient
    {
        private Customer _customer;
        private HttpClient _client;
        private Random _random = new Random();

        public ConsoleClient()
        {
            _client = new HttpClient
            {
                BaseAddress = new Uri(new ConsoleSettings().WebAPI)
            };
        }


        /// <summary>
        /// Создание нового пользователя.
        /// </summary>
        public int GenerateCustomer()
        {
            _customer = RandomCustomerGenerator.Generate(1).Single();
            _customer.Id = _random.Next(1, 10);
            return _customer.Id;
        }


        /// <summary>
        /// Добавление пользователя в БД.
        /// </summary>
        public async Task AddCustomerAsync()
        {
            var content = new StringContent(JsonConvert.SerializeObject(_customer), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync("api/Customers", content);
            Console.WriteLine((int)response.StatusCode + "." + response.StatusCode);
        }


        /// <summary>
        /// Получение пользователя из БД по Id.
        /// </summary>
        /// <param name="id">Идентификатор пользователя.</param>
        public async Task GetCustomerAsync(int id)
        {
            //var response = await _client.GetAsync($"customers/_doc/{id}");
            var response = await _client.GetAsync($"api/Customers/{id}");
            await PrintResultAsync(response);
     
        }


        /// <summary>
        /// Вывод информации по ответу от сервера.
        /// </summary>
        /// <param name="response">Ответ сервера.</param>
        async Task PrintResultAsync(HttpResponseMessage response)
        {
            var result = await response.Content.ReadAsStringAsync();
            Console.WriteLine($"Результат операции: {result}");
            Console.WriteLine($"Код ответа: {(int)response.StatusCode}.{response.StatusCode}\n");
        }

    }
}
