using Otus.Teaching.Concurrency.Import.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomer(Customer customer);
        Task<bool> AddCustomerAsync(Customer customer);
        Task<Customer> GetCustomerAsync(int id);
        IEnumerable<Customer> GetAllCustomers();
    }
}