using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    public class DataLoader : IDataLoader
    {
        private int _maxThreads;
        private string _dataFile;
        public List<long> _offset;
        private CustomerRepository _elastic;
        private MySettings _settings;
        private char[] _charsToTrim = { '\r', '\n' };


        public DataLoader(MySettings settings)
        {
            _settings = settings;
            _dataFile = settings.DataFile + ".csv";
            _elastic = new CustomerRepository(settings.ElasticConnection);
            _offset = new List<long>();
        }


        /// <summary>
        /// �������� ������ �� CSV-�����.
        /// </summary>
        public void LoadData()
        {                  
            try
            {
                // ������ ������ ���������, ����� ��������� ��� ������ ���������.
                _elastic.DropIndex("customers");
                using FileStream fileStream = new FileStream(_dataFile, FileMode.Open);
                fileStream.Seek(0, SeekOrigin.Begin);
                if (_settings.MultiThreaded)
                {
                    SetThreadPool();
                    FilePart[] _args = new FilePart[_offset.Count - 1];
                    for (int i = 0; i < _offset.Count - 1; i++)
                    {
                        _args[i] = new FilePart(fileStream)
                        {
                            Num = i
                        };
                    }
                    for (int i = 0; i < _offset.Count - 1; i++)
                    {
                        ThreadPool.QueueUserWorkItem(LoadPart, _args[i]);  
                    }
                    WaitHandle[] waitHandles = _args.Select(x => x.WaitHandle).ToArray();
                    WaitHandle.WaitAll(waitHandles);
                }
                else
                {
                    LoadPartRecords(fileStream, 0);
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }


        /// <summary>
        /// ������ ������ ��� ������ ����� ����� �����.
        /// </summary>
        /// <param name="state"></param>
        private void LoadPart(object state)
        {
            var args = (FilePart)state;
            var autoResetEvent = (AutoResetEvent)args.WaitHandle;      
            LoadPartRecords(args.FileStream, args.Num);
            autoResetEvent.Set();
        }


        /// <summary>
        /// ���������������� ������ ������� ����� ����� �����.
        /// </summary>
        /// <param name="fileStream">���������� ����� ��� ������.</param>
        /// <param name="partNum">����� ����� �����.</param>
        private void LoadPartRecords(FileStream fileStream,  int partNum)
        { 
            Console.WriteLine($"������ �������� ������ ����� {partNum + 1} ...");
            long border = _offset[partNum];
            RecordPosition record;
            while (true)
            {
                lock(fileStream)
                {
                    record = ReadLine(fileStream, border);
                }                            
                border = record.Offset;
                if(_settings.ConsoleMode)
                {
                    // ��������� ������ �� �������
                    Console.WriteLine(record.Content + $". ����� {partNum + 1}");
                }
                else
                {
                    // ��������� ������ � Elasticsearch
                    _elastic.AddCsv(record.Content);
                }             
                if (_offset[partNum + 1] == border)
                    break;
            }
            Console.WriteLine($"�������� ������ ����� {partNum + 1} ���������.");
        }


        /// <summary>
        /// �������������� �������� ������� ������ ����� ��� ������ � ������ ���������� �������.
        /// </summary>
        public void OffsetCounting()
        {            
            using FileStream fileStream = new FileStream(_dataFile, FileMode.Open);
            fileStream.Seek(0, SeekOrigin.Begin);
            // �������� ��� 1 ����� �����
            _offset.Add(0);
            if (_settings.MultiThreaded)
            {                
                // ��������� ����� 1 ����� �����
                long partLength = Convert.ToInt64(fileStream.Length / _maxThreads);
                long border = 0;                
                for (int i = 1; i < _maxThreads; i++)
                {
                    border += partLength;
                    var record = ReadLine(fileStream, border);
                    border = record.Offset;
                    if (border >= fileStream.Length)
                    {
                        // ��������� ������� ������ �� �������, �.�. ��-�� �������� ������ ������ ��������� ����� � ������� '\n' ���� ��������. 
                        // ��������� ������ ��� ��������� ������.
                        break;
                    }
                    _offset.Add(border);
                }
            }
            // �������� �� ������ ����� �� �����
            _offset.Add(fileStream.Length);
        }


        /// <summary>
        /// ������ ������ CSV-�����.
        /// </summary>
        /// <param name="fileStream">���������� ����� ��� ������.</param>
        /// <param name="border">������� ������ ������ ������.</param>
        /// <returns>����������� ������ � ����� ������� ��� ������ ��������� ������.</returns>
        private RecordPosition ReadLine(FileStream fileStream, long border)
        {            
            fileStream.Seek(border, SeekOrigin.Begin);
            List<byte> result = new List<byte>();
            int data;
            while (true)
            {
                data = fileStream.ReadByte();
                ++border;
                if (data == -1)
                    break; 
                result.Add((byte)data);
                // ������ '\n' ��������� ������.
                if (data == 10)
                    break;
            }
            return new RecordPosition
            {
                Content = Encoding.UTF8.GetString(result.ToArray()).TrimEnd(_charsToTrim),
                Offset = border
            };
        }


        /// <summary>
        /// ��������� ������������ � ������������� ���������� ������� � ���� �������.
        /// </summary>
        public void SetThreadPool()
        {
            bool setResult; 
            // Get the current settings.
            ThreadPool.GetMinThreads(out int minWorker, out int minIOC);
            if (minWorker < Environment.ProcessorCount)
            {
                minWorker = Environment.ProcessorCount;
                minIOC = Environment.ProcessorCount;
                setResult = ThreadPool.SetMinThreads(minWorker, minIOC);
                if (!setResult)
                {
                    throw new ValidationException("The minimum number of threads was not changed.");
                }
            }
            // Set the max value.
            if (_settings.MaxThreads < Environment.ProcessorCount)
            {
                _maxThreads = Environment.ProcessorCount;
            }
            else
            {
                _maxThreads = _settings.MaxThreads;
            }
            setResult = ThreadPool.SetMaxThreads(_maxThreads, _maxThreads);
            if (!setResult)
            {
                throw new ValidationException("The maximum number of threads was not changed.");
            } 
        }

    }
}