﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Core.Settings
{
    public class MySettings
    {
        /// <summary>
        /// CSV-файл для обработки.  
        /// </summary>
        public string DataFile { get; set; }

        /// <summary>
        /// Путь к генератору CSV-файла в виде отдельного приложения.
        /// </summary>
        public string GeneratorPath { get; set; }

        /// <summary>
        /// Количество генерируемых записей.
        /// </summary>
        public int DataCount { get; set; }

        /// <summary>
        ///  Максимальное количество потоков.
        /// </summary>
        public int MaxThreads { get; set; }

        /// <summary>
        /// Признак использования многопоточной обработки.
        /// </summary>
        public bool MultiThreaded { get; set; }

        /// <summary>
        /// Строка подключения к Elasticsearch.
        /// </summary>
        public string ElasticConnection { get; set; }

        /// <summary>
        /// Признак вывода полученных строк: на консоль / в БД.
        /// </summary>
        public bool ConsoleMode { get; set; }

        /// <summary>
        /// Признак выбора типа CSV-генератора: процесс / метод.
        /// </summary>
        public bool GeneratorAsProcess { get; set; }

        /// <summary>
        /// Количество допустимых попыток запуска обработчика.
        /// </summary>
        public int AttemptsTotal { get; set; } 

        public MySettings(string[] args)
        {
            DataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers");            
            GeneratorPath = @"D:\OTUS\C#\HW22\Otus.Teaching.Concurrency.Import\Otus.Teaching.Concurrency.Import.DataGenerator.App\bin\Release\netcoreapp3.1\Otus.Teaching.Concurrency.Import.DataGenerator.App.exe";
            
            IConfiguration AppConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args)
                .Build();

            DataCount = Convert.ToInt32(AppConfiguration.GetSection("DataCount").Value);           
            MaxThreads = Convert.ToInt32(AppConfiguration.GetSection("MaxThreads").Value);
            MultiThreaded = Convert.ToBoolean(AppConfiguration.GetSection("MultiThreaded").Value);
            ElasticConnection = Convert.ToString(AppConfiguration.GetSection("ElasticConnection").Value);
            ConsoleMode = Convert.ToBoolean(AppConfiguration.GetSection("ConsoleMode").Value);           
            GeneratorAsProcess = Convert.ToBoolean(AppConfiguration.GetSection("GeneratorAsProcess").Value);
            AttemptsTotal = Convert.ToInt32(AppConfiguration.GetSection("AttemptsTotal").Value); 
        }

    }
}
