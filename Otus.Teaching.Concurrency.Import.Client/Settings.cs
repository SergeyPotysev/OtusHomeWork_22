﻿using Microsoft.Extensions.Configuration;
using System;

namespace Otus.Teaching.Concurrency.Import.Client.Settings
{
    public class ConsoleSettings
    {
        /// <summary>
        /// Строка подключения к Web API.
        /// </summary>
        public string WebAPI { get; set; }

        public ConsoleSettings()
        {
            IConfiguration AppConfiguration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            WebAPI = Convert.ToString(AppConfiguration.GetSection("MyWebAPI").Value);
        }

    }
}
