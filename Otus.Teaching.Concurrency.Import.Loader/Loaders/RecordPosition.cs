﻿namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class RecordPosition
    {
        public string Content { get; set; }
        public long Offset { get; set; }
    }
}
