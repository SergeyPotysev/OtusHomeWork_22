﻿using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        static void Main(string[] args)
        {
            MySettings settings = new MySettings(args);
            if (args != null && args.Length == 1)
            {
                settings.DataFile = args[0];
                settings.DataCount = Convert.ToInt32(args[1]);
            }

            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            // Генерация CSV-файла    
            if(settings.GeneratorAsProcess)
            {
                if(!File.Exists(settings.GeneratorPath))
                {
                    Console.WriteLine("Необходимо собрать Release проекта Otus.Teaching.Concurrency.Import.DataGenerator.App");
                    Environment.Exit(-1);
                }
                GenerateAsProcess(settings);
            }
            else
            {
                GenerateCustomersDataFile(settings);
            }

            var loader = new DataLoader(settings);
            // Установка минимального и максимального значения количества потоков пула потоков
            loader.SetThreadPool();
            // Разметка частей файла
            loader.OffsetCounting();
            // Загрузка строк из файла      
            Stopwatch sw = new Stopwatch();
            sw.Start();       
            for(int n = 0; n < settings.AttemptsTotal; n++)
            {
                try
                {
                    loader.LoadData();
                    break;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + $". Попытка {n+1} из {settings.AttemptsTotal}");
                }
            }
            sw.Stop();
            Console.WriteLine("Использовано потоков: {0}", settings.MultiThreaded ? loader._offset.Count - 1 : 1);
            Console.WriteLine($"Потрачено времени: {sw.Elapsed}");
            Console.ReadKey();
        }


        /// <summary>
        /// Генерация CSV-файла через вызов процесса. 
        /// </summary>
        static void GenerateAsProcess(MySettings settings)
        {
            var proc = Process.Start(settings.GeneratorPath, $"{settings.DataFile} {settings.DataCount}");
            Console.WriteLine($"Generator started with process Id {proc.Id}...");
            proc.WaitForExit();
        }


        /// <summary>
        /// Генерация CSV-файла через вызов метода.
        /// </summary>
        static void GenerateCustomersDataFile(MySettings settings)
        {
            var CsvGenerator = new CsvGenerator(settings.DataFile, settings.DataCount);
            CsvGenerator.Generate();
        }

    }
}