﻿using System.IO;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    public class FilePart
    {
        public FileStream FileStream { get; set; }
        public int Num { get; set; }
        public WaitHandle WaitHandle { get; private set; }

        public FilePart(FileStream fileStream)
        {
            FileStream = fileStream;
            WaitHandle = new AutoResetEvent(false);
        }
    }
}
