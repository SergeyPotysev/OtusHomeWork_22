﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Client
{
    internal class Program
    {
        private static async Task Main()
        {
            int id;
            ConsoleClient client = new ConsoleClient();

            // Добавление пользователей в БД ElasticSearch 
            Console.WriteLine(" № Id Результат добавления");
            for (int n = 1; n <= 20; n++)
            {
                id = client.GenerateCustomer();
                Console.Write("{0, -3} {1, -2}", n, id);
                await client.AddCustomerAsync();
            }
            Console.WriteLine();

            // Чтение пользователей из БД по id
            while (true)
            {
                int number = ConsoleReader();
                if (number == 0)
                    break;
                await client.GetCustomerAsync(number);
            }

        }


        private static int ConsoleReader()
        {
            string stroka, digits;
            int result;
            while (true)
            {
                Console.Write("Введите Id пользователя или 0 для выхода: ");
                stroka = Console.ReadLine();
                digits = string.Join("", stroka.Where(c => char.IsDigit(c)));
                if (digits.Length > 0)
                {
                    int.TryParse(digits, out result);
                    break;
                }
            }
            return result;
        }

    }
}
