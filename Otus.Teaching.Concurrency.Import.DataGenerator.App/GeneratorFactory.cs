using Otus.Teaching.Concurrency.Import.Core.Generators;
using CsvDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.CsvGenerator;

namespace Otus.Teaching.Concurrency.Import.CsvGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(string fileName, long dataCount)
        {
            return new CsvDataGenerator(fileName, dataCount);
        }
    }
}